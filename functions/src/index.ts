import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import * as nodemailer from 'nodemailer';
import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as cors from 'cors';

admin.initializeApp(functions.config().firebase)

const gmailEmail = functions.config().gmail.email
const gmailPassword = functions.config().gmail.password
const mailTransport = nodemailer.createTransport({
	service: 'gmail',
	auth: {
		user: gmailEmail,
		pass: gmailPassword,
	},
});

const app = express()

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());

app.use('/contactus', (req, res) => {
	const email = req.body.email
	const phone = req.body.phone
	const name = req.body.name
	const message = req.body.message

	console.log(req.body)

	if(email && phone && name && message) {
		res.status(200).json({ message: "Your query has been submitted successfully" })
		return sendEmail(email, phone, name, message)
	}
	else {
		res.status(400).json({ message: "Please fill up all the necessary information" })
	}
})

app.use('/certificate', (req, res) => {
	const aadharNo = req.body.aadhar
	if(!aadharNo) {
		res.status(400).send([])
	}

	let course_promise = admin.firestore().collection('courses').where("aadhar", "==", aadharNo).get()
	let student_promise = admin.firestore().collection('students').where("aadhar", "==", aadharNo).get()

	Promise.all([student_promise, course_promise])
		.then(querySnapshot => {
			console.log(querySnapshot)
			
			let student = {}
			querySnapshot[0].forEach(students => {	
				let { name, address } = students.data() 
				student = { name, address} 
			})

			const courses = []
			querySnapshot[1].forEach((course) => {
				courses.push(course.data())
			})
			res.send({student, courses})
		})
		.catch(error => {
			console.log("Error", error)
			res.status(400).send([])
		})
})

export const widgets = functions.https.onRequest(app);

function sendEmail(email, phone, name, message) { 
	// Building Email message.
	const mailOptions = {
		from: '"Human Touch." <noreply@firebase.com>',
		to: "humantouchchamba@gmail.com",
		subject: `${name} has sent a new message`,
		html: `
			<p><strong>Message</strong> : ${message}</p>
			<p><strong>Name</strong> : ${name}</p>
			<p><strong>Email</strong> : ${email}</p>
			<p><strong>Phone</strong> : ${phone}</p>
		`
	};
	
	return mailTransport.sendMail(mailOptions)
		.then(() => console.log("User message has successfully been mailed to admin"))
		.catch((error) => console.error('There was an error while sending the email:', error));
}