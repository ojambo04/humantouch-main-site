$(document).ready(function() {
	$(document).ajaxStart(function(){
		$("#wait").css("display", "block");
		$("#search-btn").css("display", "none");
    });
    $(document).ajaxComplete(function(){
		$("#wait").css("display", "none");
		$("#search-btn").css("display", "block");
    });
	$('#searchForm').submit(function(event) {
		event.preventDefault();
		$('#result').hide();
		$('#empty-result').html('');
		$('#error').html('');
		$('#result-table tbody').empty()

		var aadhar = $("input#aadhar").val();
		
		$.ajax({
			type: "POST",
			url: "https://us-central1-humantouch-b7e0c.cloudfunctions.net/widgets/certificate",
			data: {
				aadhar: aadhar
			},
			cache: false,
			success: function(data) {
				if(data) {
					$('#result').show();
					$("#student_info").html("<p><strong>Name:</strong>&nbsp;"+ data.student.name + 
						", &nbsp;&nbsp;&nbsp;<strong>Address:</strong>&nbsp;" + data.student.address +"</p>")

					data.courses.forEach(course => {
						var markup = "<tr><td>" + course.name + "</td><td>" + course.training_agency + 
							"</td><td>" + course.training_center + "</td><td>" + course.grade_obtained + "</td>"
						if(course.fileUrl) {
							markup = markup + "<td><a target='_blank' class='download btn btn-success' href='" + 
							course.fileUrl + "'>Download</a></td></tr>"
						} else {
							markup = markup + "<td>Unavailable</td></tr>"
						}					
						
						$('#result-table tbody').append(markup)
					});	
				} 
				else {
					$('#empty-result').html('<p class="alert alert-warning">Couldnt find any student record with that Aadhar No. Please try again!!</p>');
				}
			},
			error: function() {
				$('#error').html("<p class='alert alert-danger'>There is some problem with the server. Please try again later!!</p>");
			}
		});

	});
})
